
# Object Orientated Programming
[[toc]]
## Overview
Object orientation is designed to allow multiple things to happen at the same time.  An example may be a bank where each user is an object, and they all have the same code, with different values, that therefore can have a different output based on the input.
Objects can have different attributes.  In the case of a pen, an attribute could be colour, size, appearance ect.
### Definitions
* Class - A new data-type.
* Constructor - The code that calls the object and instantiates it.  Cannot be called from outside the class.
## Base Class
A base class is a master class for any class that inherits it.  A Biro pen maybe the master class, as it is the most basic pen.  Any other pen may inherit the biro and expand it.
## Improvements to Procedure Orientated Programming
* Encapsulation - a programming language mechanism for restricting direct access for IT-Developers to some of the object's data.  The object Student can have public variable knowlege_level and private variable critical_information.
* Aggregation - an aggregate object is an object which contains other objects.  For example, a Car class would contain Engine, Wheels, Cabin and fuel objects.  Sometimes, a class refers to real-world physical objects (like a Car).  Sometimes it is more abstract (e.g. university and students).
* Composition - composition is like aggregation but it is a real-time technique.  Using interfaces you can replicate objects at real time, but objects must have the same type.
* Interfaces - the interface separates the implementation  and defines the structure.  It's like a car's digital-audio panel - where you can choose between rock, classical music or a radio station.  This concept is useful when. implementation of the object can be interchangeable.  Also, pay attention that you can use this technique if implementation is changing frequently.
## Stages to writing Object Oriented Code
1. Create a master class.  This contains the attributes for any of the objects and any methods that all the objects should have. The master class can be thought of the DNA of the objects
2. Instantiate an object
3. Assign the attributes to the object
Despite the objects having the same methods, they are Encapsulated and treated as different pieces of data.  This means you can have multiple objects of the same type without their variables clashing
### Running Object orientated code
The code is dormant before running the code.  Once the object is instantiated then the object exists.  This means that the code that the object holds will not run unless the object exists.  If one object has 1 million lines of code, but is not instantiated, the code will not run.
## Polymorphism
In procedural code, data available to one method is generally available to all methods.  In Object Orientated code, the visibility of data can be manually restricted.
### Protection Levels
C Sharp/Visual Basic:
```csharp
private // Data only seen inside the class/struct
public // Data publicly accessable from outside the class/struct
protected // Data can only be accessed by code in the same class or struct, or in a derived class
```
## Overloading
Most OO languages allow the standard mathematical operators to be redefined.
## Code example
A program needs to do maths to do with all the planets.  Each planet is stored as an object.
Planet.cs (Base class)
```csharp
public class Planet
{
    // Variables for each property of the planet
    public string PlanetName { get; set; }
    public double PlanetMass { get; set; }
    public double PlanetDistance { get; set; }
    public double CentreOfMass { get; set; }

    /* Instance constructor that requires the name of the planet as a parameter
     * The mass of the planet and distance can also be provided when the planet is instantiated but is not required */
    public Planet(string name, double mass = 0, double distance = 0)
    {
        PlanetName = name;
        PlanetMass = mass;
        PlanetDistance = distance;
    }
}
```
Main.cs (Where the objects are instantiated)
```csharp
public static Planet Mercury = new Planet("Mercury", 0.330E24, 57.9E9);
public static Planet Venus = new Planet("Venus", 4.87E24, 108.2E9);
public static Planet Earth = new Planet("Earth", 5.97E24, 149.6E9);
public static Planet Mars = new Planet("Mars", 0.642E24, 227.9E9);
public static Planet Jupiter = new Planet("Jupiter", 1898E24, 778.6E9);
public static Planet Saturn = new Planet("Saturn", 568E24, 1433.5E9);
public static Planet Uranus = new Planet("Uranus", 86.8E24, 2872.5E9);
public static Planet Neptune = new Planet("Neptune", 102E24, 4495.1E9);
public static Planet Pluto = new Planet("Pluto", 0.0146E24, 5906.4E9);
```
The entire project can be found [here](https://bitbucket.org/thomassaunders/solar-calculator/src/master/).